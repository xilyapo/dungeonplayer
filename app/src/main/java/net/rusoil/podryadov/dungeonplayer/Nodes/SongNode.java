package net.rusoil.podryadov.dungeonplayer.Nodes;


import android.graphics.Bitmap;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

public class SongNode extends Node implements Serializable {
    String src;
    public int duration;

    public transient Bitmap itemBitmap;
    public String getSrc() {
        return src;
    }
    public SongNode(String src,FolderNode parent) {
        super(new ArrayList<NodeChip>(),new File(src).getName().replaceFirst("[.][^.]+$", ""),0,"#000000", "#FFFFFF",null);
        this.src=src;
        this.parent = parent;
        type = "Song";
    }

    public SongNode(String src,String name, FolderNode parent,Bitmap bitmap,int duration) {
         super(new ArrayList<NodeChip>(),new File(src).getName().replaceFirst("[.][^.]+$", ""),0,"#000000", "#FFFFFF",null);
        this.src=src;
        this.parent = parent;
        this.name = name;
        this.duration = duration;
        type = "Song";
        this.itemBitmap = bitmap;
    }

    @NonNull
    @Override
    public Object clone()  {
        SongNode node = new SongNode(src,name,parent,itemBitmap,duration);
        return node;
    }
}
