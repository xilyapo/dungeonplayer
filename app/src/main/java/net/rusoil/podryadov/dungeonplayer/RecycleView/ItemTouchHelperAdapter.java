package net.rusoil.podryadov.dungeonplayer.RecycleView;

public interface ItemTouchHelperAdapter {
    void onItemMove(int fromPosition, int toPosition);
    void onItemSwiped(int position,int direction);
}
