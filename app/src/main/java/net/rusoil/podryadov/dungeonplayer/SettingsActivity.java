package net.rusoil.podryadov.dungeonplayer;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.developer.filepicker.controller.DialogSelectionListener;
import com.developer.filepicker.model.DialogConfigs;
import com.developer.filepicker.model.DialogProperties;
import com.developer.filepicker.view.FilePickerDialog;

import java.io.File;

public class SettingsActivity extends AppCompatActivity {
    public static final String RADIOBUTTON_STATE_PREFERENCE = "0";
    public static final String SELECTED_FOLDER_PREFERENCE = "1";
    public static final String PLAYER_STYLE_PREFERENCE = "2";
    private static final int REQUEST_FILE_READWRITE_PERMISSION = 0;
    TextView directory;
    RadioGroup radioButtonGroup;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        directory = findViewById(R.id.directoryTextView);
        radioButtonGroup = findViewById(R.id.radioButtonGroup);
        preferences = getSharedPreferences("settings", MODE_PRIVATE);
        radioButtonGroup.check(preferences.getBoolean(RADIOBUTTON_STATE_PREFERENCE, true) ? R.id.SD_radioButton : R.id.IntMemory_radioButton);
        directory.setText(preferences.getString(SELECTED_FOLDER_PREFERENCE, "Нет рабочей директории"));
    }

    private void initDialog(String pathname) {
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.DIR_SELECT;
        properties.root = new File(pathname);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;
        properties.show_hidden_files = false;
        dialog = new FilePickerDialog(SettingsActivity.this, properties);
        dialog.setTitle("Выберите директорию");

        dialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                directory.setText(files[0]);
                preferences.edit().putString(SELECTED_FOLDER_PREFERENCE, files[0]).apply();
                preferences.edit().putBoolean(RADIOBUTTON_STATE_PREFERENCE, radioButtonGroup.getCheckedRadioButtonId() == R.id.SD_radioButton).apply();


            }
        });
    }

    FilePickerDialog dialog;
    DialogProperties properties = new DialogProperties();

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case FilePickerDialog.EXTERNAL_READ_PERMISSION_GRANT: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (dialog != null) {
                        dialog.show();
                    }

                } else {
                    Toast.makeText(SettingsActivity.this, "Permission is required to select music directory.", Toast.LENGTH_LONG).show();
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    public void btnSelect(View view) {
        int readPermissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (readPermissionStatus == PackageManager.PERMISSION_GRANTED) {
            if (radioButtonGroup.getCheckedRadioButtonId() == R.id.SD_radioButton) {
                initDialog("/storage/");
                dialog.show();

            } else {
                initDialog(DialogConfigs.DEFAULT_DIR);
                dialog.show();
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_FILE_READWRITE_PERMISSION
            );
            Toast.makeText(SettingsActivity.this, "Permission is required to select music directory.", Toast.LENGTH_LONG).show();
        }

    }

    private void selectFolder() {

    }
}




