package net.rusoil.podryadov.dungeonplayer.Nodes;

import android.content.Context;

import net.rusoil.podryadov.dungeonplayer.Util.NodeUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FolderNode extends Node implements Serializable {
    public List<Node> items;
    public FolderNode(String name, ArrayList<Node> items, FolderNode parent) {
        this.items = items;
        this.name = name;
        type = "Folder";
        this.parent = parent;
    }

    public FolderNode clone(Context context) {
        NodeUtils.Serialization.saveNodeGSON(this,context.getFilesDir().getAbsolutePath(),"temp.json");
        FolderNode node = NodeUtils.Serialization.loadNodeGson(context,context.getFilesDir().getAbsolutePath(),"temp.json");
        node.parent = parent;
        node.name = name;
        return node;
    }

    public FolderNode() {
        type = "Folder";
    }

    public FolderNode(String name, FolderNode parent) {
        this.name = name;
        this.items = new ArrayList<Node>();
        type = "Folder";
        this.parent = parent;
    }
    public FolderNode(String name) {
        this.name = name;
        this.items = new ArrayList<Node>();
        type = "Folder";
        this.parent = null;
    }
    public Node getNode(int pos){
        if (items.size()-1<pos){
            return items.get(pos);
        }
        return null;
    }
}
