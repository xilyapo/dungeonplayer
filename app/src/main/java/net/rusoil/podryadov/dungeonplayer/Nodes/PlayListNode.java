package net.rusoil.podryadov.dungeonplayer.Nodes;

import java.io.Serializable;
import java.util.ArrayList;

public class PlayListNode extends FolderNode implements Serializable {
    public int current = 0;
    public PlayListNode(String name,ArrayList<Node> items,FolderNode parent) {
        this.items = items;
        type = "Playlist";
        this.parent = parent;
    }

    public SongNode next(){
        if (current==items.size()-1)
            return null;
        return (SongNode) items.get(current++);
    }

}
