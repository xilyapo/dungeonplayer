package net.rusoil.podryadov.dungeonplayer.Nodes;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class NodeChip implements Serializable {
    public String type = "Chip";
    public String name;
    public int textColor;
    //public int backgroundColor;
    public NodeChip(String name, int textColor, int backgroundColor) {
        this.name = name;
        this.textColor = textColor;
        //this.backgroundColor = backgroundColor;
    }
    public NodeChip(String name, int textColor) {
        this.name = name;
        this.textColor =  textColor;
    }

    @Override
    public boolean equals(@Nullable Object obj)  {
        NodeChip chip = (NodeChip) obj;
        return chip.name.equals(name);
    }
}
