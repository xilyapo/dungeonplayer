package net.rusoil.podryadov.dungeonplayer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import net.rusoil.podryadov.dungeonplayer.Nodes.NodeChip;
import net.rusoil.podryadov.dungeonplayer.Nodes.FolderNode;
import net.rusoil.podryadov.dungeonplayer.Nodes.Node;
import net.rusoil.podryadov.dungeonplayer.Nodes.SongNode;
import net.rusoil.podryadov.dungeonplayer.RecycleView.ItemTouchHelperCallback;
import net.rusoil.podryadov.dungeonplayer.Util.NodeUtils;
import net.rusoil.podryadov.dungeonplayer.RecycleView.ItemAdapter;
import net.rusoil.podryadov.dungeonplayer.Util.SongUtils;

import java.util.List;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, MediaPlayer.OnPreparedListener,MediaPlayer.OnCompletionListener,MediaPlayer.OnErrorListener {
    RecyclerView recyclerView;
    ItemAdapter adapter;
    MediaPlayer mediaPlayer;
    SharedPreferences preferences;
    TextView songName;
    TextView songTime;
    ChipGroup chipGroup;
    ImageView songMiniature;
    public ImageButton playBtn;
    SeekBar bar;
    ImageButton expandButton;
     FolderNode rootNode;
    TextView currentFolderText;
    boolean isLoopMode = false;
    boolean isContinuedMode = true;
    public List<Node> currentPlaylist;
    int playlistPos = -1;
    public static final int REQUEST_CODE_CUSTOM = 1;
    final String JSON_FILE_NAME = "output.json";
    public static String currentFilesFolder;
    Handler mHandler;

    @Override
    protected void onStop() {
        NodeUtils.Serialization.save_async(this,rootNode);
        super.onStop();
    }

    @Override
    protected void onStart() {
        currentFilesFolder = preferences.getString(SettingsActivity.SELECTED_FOLDER_PREFERENCE,null);
        List<String> songs = SongUtils.parseSongsSrc(this,currentFilesFolder);
        if (currentFilesFolder!=null){
            SongUtils.removeDif(this,rootNode,songs);
            SongUtils.mergeDif(this,rootNode,songs);
        }

        Log.e("@!!!",songs.toString());
        if (rootNode==null)
            rootNode = NodeUtils.Serialization.resetRootNode(this,currentFilesFolder);
        adapter.setCurrentNode(rootNode);
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audio_controller);
        preferences = getSharedPreferences("settings", MODE_PRIVATE);
        currentFilesFolder = preferences.getString(SettingsActivity.SELECTED_FOLDER_PREFERENCE,null);
        rootNode = NodeUtils.Serialization.load_sync(this);
        List<String> songs = SongUtils.parseSongsSrc(this,currentFilesFolder);

        Log.e("@!!!",songs.toString());
        if (rootNode==null)
            rootNode = NodeUtils.Serialization.resetRootNode(this,currentFilesFolder);
        //node = NodeUtils.Serialization.loadNodeGson(this,getFilesDir().getAbsolutePath() ,JSON_FILE_NAME);
        //if (node!=null){
        //    setRecyclerView(node);
         //   rootNode = node;
         //    save_json_async();
        //}
        //else {
            //rootNode = NodeUtils.Serialization.resetRootNode(this,currentFilesFolder);
            setRecyclerView(rootNode);
        //}
        initLayout();
        PrepareMediaPlayer();
        mHandler = new Handler();
        MainActivity.this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                try {
                    if (mediaPlayer != null&& mediaPlayer.isPlaying()) {
                        int mCurrentPosition = mediaPlayer.getCurrentPosition() / 1000;
                        bar.setProgress(mediaPlayer.getCurrentPosition());
                        long total = mediaPlayer.getDuration();
                        long current = mediaPlayer.getCurrentPosition();
                        int hours = (int) (total / (1000 * 60 * 60));
                        int minutes = (int) (total % (1000 * 60 * 60)) / (1000 * 60);
                        int seconds = (int) ((total % (1000 * 60 * 60)) % (1000 * 60) / 1000);
                        songTime.setText(String.format("%s / %s", milliSecondsToTimer(current), milliSecondsToTimer(total)));
                    }
                }catch (IllegalStateException exception){
                    exception.printStackTrace();
                }
                mHandler.postDelayed(this, 1000);
            }

            private String milliSecondsToTimer(long milliseconds) {
                String finalTimerString = "";
                String secondsString = "";

// Convert total duration into time
                int hours = (int) (milliseconds / (1000 * 60 * 60));
                int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
                int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
// Add hours if there
                if (hours > 0) {
                    finalTimerString = hours + ":";
                }

// Prepending 0 to seconds if it is one digit
                if (seconds < 10) {
                    secondsString = "0" + seconds;
                } else {
                    secondsString = "" + seconds;
                }

                finalTimerString = finalTimerString + minutes + ":" + secondsString;

// return timer string
                return finalTimerString;
            }
        });
        setCurrentFolderText(rootNode.name);

    }

    private void initLayout() {
        Button btn;
        currentFolderText = findViewById(R.id.folder_header);
        ImageButton addFolderBtn = findViewById(R.id.addFolderBtn);
        addFolderBtn.setOnClickListener(this::onClick);
        songMiniature = findViewById(R.id.audio_preview_img);
        songName = findViewById(R.id.trackName_textView);
        chipGroup = findViewById(R.id.main_chipGroup);
        songTime = findViewById(R.id.timeCodeTextView);
        playBtn = findViewById(R.id.play_btn);

        playBtn.setOnClickListener(this::onClick);
        expandButton = findViewById(R.id.change_mode);
        expandButton.setOnClickListener(this::onClick);
        findViewById(R.id.forward_btn).setOnClickListener(this::onClick);
        findViewById(R.id.backward_btn).setOnClickListener(this::onClick);
        findViewById(R.id.next_btn).setOnClickListener(this::onClick);
        findViewById(R.id.previous_btn).setOnClickListener(this::onClick);
        bar = findViewById(R.id.seekBar);
        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(mediaPlayer != null && fromUser) {
                    if (mediaPlayer.isPlaying())
                        playBtn.setImageResource(android.R.drawable.ic_media_pause);
                    mediaPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (mediaPlayer != null && mediaPlayer.getCurrentPosition() > 1) {
                    playBtn.setImageResource(android.R.drawable.ic_media_pause);
                    mediaPlayer.seekTo(0);
                    mediaPlayer.start();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

    }
    private void PrepareMediaPlayer() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioAttributes(
                new AudioAttributes
                        .Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build());
        mediaPlayer.setOnPreparedListener(this::onPrepared);
        mediaPlayer.setOnCompletionListener(this::onCompletion);
        mediaPlayer.setOnErrorListener(this::onError);

    }
    private void setRecyclerView(FolderNode root) {
        recyclerView = findViewById(R.id.musicRecyclerView);
        adapter = new ItemAdapter(this,root);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(adapter, adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
    }
    public void assignChips(List<NodeChip> nodeChips) {
        chipGroup.removeAllViews();
        if (nodeChips != null) {
            for (NodeChip item : nodeChips
            ) {
                com.google.android.material.chip.Chip chip = new com.google.android.material.chip.Chip(this.chipGroup.getContext());
                chip.setText(item.name);
                //chip.setBackgroundColor(item.backgroundColor);
                chip.setTextColor(item.textColor);
                chip.setOnClickListener(this::onChipClick);
                chipGroup.addView(chip);
            }
        }
    }
    public void onChipClick(View v){
        Chip chip = (Chip) v;
        NodeChip chip1 = new NodeChip(chip.getText().toString(),0,0);
        FolderNode searchNode = NodeUtils.searchAllFolders(chip1,rootNode);
        currentFolderText.setText(searchNode.name);
        adapter.setCurrentNode(searchNode);
    }

    public void playSound(int position, List<Node> playList) {
        if (!(playList.get(position) instanceof SongNode))
            return;
        SongNode node = (SongNode) playList.get(position);

        Bitmap image = node.itemBitmap;
        if (image == null)
            songMiniature.setImageResource(R.drawable.empty_miniature);
        else
            songMiniature.setImageBitmap(image);
        assignChips(node.nodeChips);
        playBtn.setImageResource(android.R.drawable.ic_media_pause);
        songName.setText(node.name);
        try {
            mediaPlayer.reset();
            mediaPlayer.setDataSource(node.getSrc());
            mediaPlayer.prepareAsync();
            if (isContinuedMode){
                currentPlaylist = playList;
                playlistPos=position;
            }
        } catch ( Exception exception) {
            exception.printStackTrace();
        }

    }
    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        mediaPlayer.reset();
        return false;
    }
    @Override
    public void onCompletion(MediaPlayer mp) {
        mediaPlayer.reset();
        bar.setProgress(0);

        if (isContinuedMode&&currentPlaylist!=null){
            playlistPos++;
            for (;playlistPos<currentPlaylist.size();playlistPos++
                 ) {
                Node node = currentPlaylist.get(playlistPos);
                if (node instanceof SongNode){
                    playSound(playlistPos,currentPlaylist);
                    return;
                }
            }

        }
        playBtn.setImageResource(android.R.drawable.ic_media_play);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        bar.setMax(mediaPlayer.getDuration());
        mediaPlayer.start();
        if (currentPlaylist== adapter.getCurrentNode().items){
            adapter.changeSelected(playlistPos);
        }
    }



    @Override
    public void onClick(View v) {
        //todo: Прописать управление треком
        switch (v.getId()) {
            case R.id.play_btn:
                if (mediaPlayer == null)
                    break;
                if (mediaPlayer.isPlaying()) {
                    playBtn.setImageResource(android.R.drawable.ic_media_play);
                    mediaPlayer.pause();
                    break;
                }
                if (!mediaPlayer.isPlaying() && mediaPlayer.getCurrentPosition() > 1) {
                    playBtn.setImageResource(android.R.drawable.ic_media_pause);
                    mediaPlayer.start();
                    break;
                }


                break;
            case R.id.next_btn:
                if (mediaPlayer == null)
                    break;
                if (++playlistPos>0 && playlistPos<currentPlaylist.size())
                    playSound(playlistPos,currentPlaylist);
                break;
            case R.id.previous_btn:
                if (mediaPlayer == null)
                    break;
                if (playlistPos>1)
                    playSound(--playlistPos,currentPlaylist);
                break;
            case R.id.forward_btn:
                if (mediaPlayer == null)
                    break;
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() + 5000);
                break;
            case R.id.backward_btn:
                if (mediaPlayer == null)
                    break;
                mediaPlayer.seekTo(mediaPlayer.getCurrentPosition() - 5000);

                break;
            case R.id.addFolderBtn:
                adapter.getCurrentNode().items.add(0, new FolderNode("Папка", adapter.getCurrentNode()));
                playlistPos++;
                adapter.changeSelected(playlistPos);
                adapter.notifyDataSetChanged();
                break;
            case R.id.change_mode:
                adapter.changeAllOpen();
                break;

        }
    }






    public void changeSettings(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }



    @Override
    public void onBackPressed() {
        if (!adapter.back())
            super.onBackPressed();
        if (currentPlaylist == adapter.getCurrentNode().items)
        {
            NodeUtils.Serialization.save_async(this,rootNode);
            adapter.changeSelected(playlistPos);
        }
        adapter.notifyDataSetChanged();
    }



    @Override
    public void onDestroy() {

        if (mediaPlayer != null) mediaPlayer.release();
        super.onDestroy();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("MyLogs","requestcode "+requestCode + ", resultCode = " + resultCode);
        if (resultCode == RESULT_OK){
            if (requestCode == REQUEST_CODE_CUSTOM){
                int position = data.getIntExtra("position",-1);
                int textColor = data.getIntExtra("newTextColor",-1);
                int backgroundColor = data.getIntExtra("newBackGroundColor",-1);
                String newName = data.getStringExtra("newName");
                adapter.changeCurrentData(position,newName,textColor,backgroundColor);


            }
        }
    }




    public void setCurrentFolderText(String text){
        currentFolderText.setText(text);
    }
    }
