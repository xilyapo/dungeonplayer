package net.rusoil.podryadov.dungeonplayer.RecycleView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import net.rusoil.podryadov.dungeonplayer.Nodes.FolderNode;
import net.rusoil.podryadov.dungeonplayer.Nodes.Node;

import java.util.List;

public class ItemTouchHelperCallback extends ItemTouchHelper.Callback {
    private final ItemTouchHelperAdapter mAdapter;
    RecyclerView.Adapter adapter ;
    int dragFrom = -1;
    int dragTo = -1;

    @Override
    public boolean isItemViewSwipeEnabled() {
        return false;
    }

    @Override
    public boolean canDropOver(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder current, @NonNull RecyclerView.ViewHolder target) {
        ItemAdapter.ViewHolder holderCur = (ItemAdapter.ViewHolder)current;
        ItemAdapter.ViewHolder holderTo = (ItemAdapter.ViewHolder)target;
        if (holderCur.getType()==ItemTypes.FOLDER&&holderTo.getType()==ItemTypes.FOLDER)
            return true;
        if (holderCur.getType()!=ItemTypes.FOLDER&&holderTo.getType()!=ItemTypes.FOLDER)
            return true;
        return false;
    }

    @Override
    public RecyclerView.ViewHolder chooseDropTarget(@NonNull RecyclerView.ViewHolder selected, @NonNull List<RecyclerView.ViewHolder> dropTargets, int curX, int curY) {
        return super.chooseDropTarget(selected, dropTargets, curX, curY);
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

    }

    @Override
    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
        super.onSelectedChanged(viewHolder, actionState);
    }

    public ItemTouchHelperCallback(ItemTouchHelperAdapter mAdapter, RecyclerView.Adapter adapter) {
        this.mAdapter = mAdapter;
        this.adapter = adapter;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.LEFT;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        int fromPosition = viewHolder.getAdapterPosition();
        int toPosition = target.getAdapterPosition();


        if(dragFrom == -1) {
            dragFrom =  fromPosition;
        }
        dragTo = toPosition;
        mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);

        if(dragFrom != -1 && dragTo != -1 && dragFrom != dragTo) {
            reallyMoved(dragFrom, dragTo);
        }

        dragFrom = dragTo = -1;
    }

    private void reallyMoved(int dragFrom, int dragTo) {
        ItemAdapter adapter1 = ((ItemAdapter)adapter);
        Node node1 = adapter1.getCurrentNode().items.get(dragFrom);
        Node node2 = adapter1.getCurrentNode().items.get(dragTo);
        if (!(node1 instanceof FolderNode) &&(node2 instanceof FolderNode)){
            ((FolderNode) node2).items.add(node1);
            node1.setParent((FolderNode) node2);
            adapter1.getCurrentNode().items.remove(node1);
            adapter1.notifyDataSetChanged();

        }
        if ((node1 instanceof FolderNode) &&(node2 instanceof FolderNode)){
            ((FolderNode) node2).items.add(node1);
            node1.setParent((FolderNode) node2);
            adapter1.getCurrentNode().items.remove(node1);
            adapter1.notifyDataSetChanged();

        }
    }

}
