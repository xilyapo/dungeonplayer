package net.rusoil.podryadov.dungeonplayer.Util;

import android.app.Application;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import net.rusoil.podryadov.dungeonplayer.MainActivity;
import net.rusoil.podryadov.dungeonplayer.Nodes.FolderNode;
import net.rusoil.podryadov.dungeonplayer.Nodes.Node;
import net.rusoil.podryadov.dungeonplayer.Nodes.SongNode;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class SongUtils {

    public static List<String> parseSongsSrc(Context context, String dirSrc){
        String[] projection = { MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.DISPLAY_NAME};

        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0 AND " +
                MediaStore.Audio.Media.DATA + " LIKE '"+dirSrc+"/%'";

        Cursor cursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection, selection, null, null);
        int dataColumn = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
        List<String> fileSrc = new ArrayList<>();
        while (cursor.moveToNext()) {
            String src = cursor.getString(dataColumn);
            fileSrc.add(src);
        }

        return fileSrc;
    }
    public static Set<String> parseSongsSrc(FolderNode folderNode){
        HashSet<String> set = new HashSet<>();
        for (Node node:folderNode.items
             ) {
            if (node instanceof SongNode)
                set.add(((SongNode) node).getSrc());
        }
        return set;
    }

    public static boolean mergeDif(Context context,FolderNode node,List<String> srcList){
        if (node !=null) {
            HashSet<String> nodeSongs = getAllSongs(node);
            for (String item : srcList
            ) {
                if (!nodeSongs.contains(item)) {
                    node.items.add(NodeUtils.formSongNode(context, node, item));
                }
            }
            return true;
        }
        return false;
    }
    public static boolean removeDif(Context context,FolderNode root,List<String> srcList){
        Queue<FolderNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()){
            FolderNode rt = queue.poll();
            Iterator<Node> iterator = rt.items.iterator();
            while (iterator.hasNext()) {
                Node node = iterator.next();
                if (node instanceof  SongNode){
                    if (srcList.contains(((SongNode) node).getSrc()));
                        iterator.remove();
                }
            }
        }
        return true;
    }
    public static HashSet<String> getAllSongs(FolderNode root){
        HashSet<String> set = new HashSet<>();
        Queue<FolderNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()){
            FolderNode rt = queue.poll();
            for (Node node:rt.items ) {
                if (node instanceof SongNode){
                    set.add(((SongNode) node).getSrc());
                }
                if (node instanceof FolderNode) {
                    queue.add((FolderNode) node);
                }
            }


        }
        return set;
    }
    public static String getTimeCode(int millis){
        String finalTimerString = "";
        String secondsString = "";

// Convert total duration into time
        int hours = (int) (millis / (1000 * 60 * 60));
        int minutes = (int) (millis % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((millis % (1000 * 60 * 60)) % (1000 * 60) / 1000);
// Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

// Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

// return timer string
        return finalTimerString;
    }
    public static Bitmap formBitmapFromByte(Context context,SongNode songNode){
        File file = new File(songNode.getSrc());
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource( context,Uri.fromFile(file));
        Bitmap art;
        BitmapFactory.Options bfo=new BitmapFactory.Options();
        byte[] rawArt = mmr.getEmbeddedPicture();
        if (null != rawArt){
            art = BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length, bfo);
            return art;
        }
        return null;
    }
}
