package net.rusoil.podryadov.dungeonplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skydoves.colorpickerview.ColorPickerView;
import com.skydoves.colorpickerview.flag.BubbleFlag;
import com.skydoves.colorpickerview.flag.FlagMode;
import com.skydoves.colorpickerview.sliders.BrightnessSlideBar;

public class CustomizeDialogActivity extends AppCompatActivity implements View.OnClickListener {
    ColorPickerView colorPickerView;
    Button changeTextColorBtn;
    Button changeBackgroundColorBtn;
    Button saveResultsBtn;
    ConstraintLayout layout;
    TextView mainText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customize_dialog);
        layout = findViewById(R.id.nav_item_layout);
        mainText = findViewById(R.id.nav_item_name);
        colorPickerView = findViewById(R.id.colorPickerView);
        changeTextColorBtn = findViewById(R.id.textClrBtn);
        changeBackgroundColorBtn = findViewById(R.id.backgroundClrBtn);
        saveResultsBtn = findViewById(R.id.saveResult);
        colorPickerView.setPaletteDrawable(AppCompatResources.getDrawable(this,R.drawable.gradient));
        colorPickerView.setSelectorDrawable(AppCompatResources.getDrawable(this,R.drawable.wheel));
        BubbleFlag bubbleFlag = new BubbleFlag(this);
        bubbleFlag.setFlagMode(FlagMode.FADE);
        colorPickerView.setFlagView(bubbleFlag);
        changeBackgroundColorBtn.setOnClickListener(this);
        changeTextColorBtn.setOnClickListener(this);
        saveResultsBtn.setOnClickListener(this);
        Intent intent =getIntent();
        position = intent.getIntExtra("position",-1);
        Log.e("MyError", String.valueOf(position));
        mainText.setText(intent.getStringExtra("text"));
        mainText.setTextColor(intent.getIntExtra("textColor", Color.BLACK));
        layout.setBackgroundColor(intent.getIntExtra("backgroundColor", Color.BLACK));
    }

    int position;
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textClrBtn:
                mainText.setTextColor(colorPickerView.getColor());
                break;
            case R.id.backgroundClrBtn:
                layout.setBackgroundColor(colorPickerView.getColor());
                break;
            case R.id.saveResult:
                Intent intent = new Intent();
                intent.putExtra("newName",mainText.getText().toString());
                intent.putExtra("newTextColor",mainText.getCurrentTextColor());
                intent.putExtra("newBackGroundColor",((ColorDrawable)layout.getBackground()).getColor());
                intent.putExtra("position",position);
                setResult(RESULT_OK,intent);
                finish();
                break;
        }

    }
}