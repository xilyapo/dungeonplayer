package net.rusoil.podryadov.dungeonplayer.Util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.rusoil.podryadov.dungeonplayer.Nodes.NodeChip;
import net.rusoil.podryadov.dungeonplayer.Nodes.FolderNode;
import net.rusoil.podryadov.dungeonplayer.Nodes.Node;
import net.rusoil.podryadov.dungeonplayer.Nodes.PlayListNode;
import net.rusoil.podryadov.dungeonplayer.Nodes.SongNode;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class NodeUtils {
    public static FolderNode searchAllFolders(NodeChip chip1, FolderNode rootNode) {
        FolderNode resultNode =new FolderNode("Тег = "+chip1.name,rootNode);
        Queue<FolderNode> queue = new LinkedList<>();
        queue.add(rootNode);
        while (!queue.isEmpty()) {
            FolderNode rt = queue.poll();
            for (Node node : rt.items
            ) {
                if (node.nodeChips.contains(chip1)){
                    resultNode.items.add(node);
                }
            }
        }
        Log.e("!!!!!!!!!!",resultNode.items.toString());
        Log.e("!!!!!!!!!!",rootNode.items.get(0).nodeChips.get(1).name.toString());
        return resultNode;
    }

    public static class Serialization {

        public static FolderNode load_sync(Context context) {
            try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File(context.getFilesDir(),"out.out"))))
            {
                FolderNode p=(FolderNode) ois.readObject();
                NodeUtils.restoreLinks(context,p);
                Log.e("GOOOOD",p.name+"  "+p.items);

                return p;
            }
            catch(Exception ex){

                System.out.println(ex.getMessage());
            }
            return null;
        }


        public static void save_async(Context context,FolderNode rootNode) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(context.getFilesDir(),"out.out"))))
                    {
                        oos.writeObject(rootNode);
                    }
                    catch(Exception ex){

                        System.out.println(ex.getMessage());
                    }
                }
            });
            t.start();
        }


        // FIXME: 24.05.2021
        public static void saveNodeGSON(FolderNode node, String src,String name){
            Gson gson = new Gson();
            try {
                File res = new File(src,name);
                FileWriter writer = new FileWriter(res);
                gson.toJson(node,FolderNode.class,writer);
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        public static FolderNode resetRootNode(Context context, String dir){
            List<String> songs = SongUtils.parseSongsSrc(context,dir);
            FolderNode root = new FolderNode("Текущий плейлист/Треки", new ArrayList<Node>(), null);
            for (String song:songs
            ) {
                root.items.add(formSongNode(context, root, song));
            }
            return root;
        }

        // FIXME: 24.05.2021
        public static FolderNode loadNodeGson(Context context,String src,String name){

                RuntimeTypeAdapterFactory<Node> nodeRuntimeTypeAdapterFactory
                        = RuntimeTypeAdapterFactory.of(Node.class, "type");
                nodeRuntimeTypeAdapterFactory
                        .registerSubtype(FolderNode.class, "Folder")
                        .registerSubtype(SongNode.class, "Song")
                        .registerSubtype(PlayListNode.class, "Playlist");
                Gson gson = new GsonBuilder().registerTypeAdapterFactory(nodeRuntimeTypeAdapterFactory).create();
                try {
                    File res = new File(src, name);
                    FileReader reader = new FileReader(res);
                    Node folderNode = gson.fromJson(reader, Node.class);
                    reader.close();
                    if (folderNode == null)
                        return null;
                    restoreLinks(context, (FolderNode) folderNode);
                    return (FolderNode)folderNode;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }

            return null;
        }
        public static FolderNode createData(){
            FolderNode node = new FolderNode("Текущая папка/Треки",new ArrayList<Node>(),null);
            addFolderItems(5,node);
            addChips(5,node.items.get(0));
            addChips(5,node.items.get(1));
            addSongItems(10,node);
            addFolderItems(5,((FolderNode)node.items.get(0)));
            addFolderItems(5,((FolderNode)node.items.get(1)));
            addFolderItems(5,((FolderNode)node.items.get(2)));
            addFolderItems(5,((FolderNode)node.items.get(3)));
            addSongItems(5,((FolderNode)node.items.get(0)));
            addSongItems(5,((FolderNode)node.items.get(1)));
            addSongItems(5,((FolderNode)node.items.get(2)));
            addSongItems(5,((FolderNode)node.items.get(3)));
            return node;
        }
        public static void addChips(int n, Node node){
            node.nodeChips = new ArrayList<NodeChip>();
            for (int i = 0; i<n;i++){
                node.nodeChips.add(new NodeChip(String.valueOf(i), Color.BLACK,Color.WHITE));
            }
        }
        public static void addFolderItems(int n,FolderNode node){
            for (int i=0;i<n;i++){
                node.items.add(new FolderNode(String.valueOf(i),node));
            }
        }
        public static void addSongItems(int n,FolderNode node){
            for (int i=0;i<n;i++){
                node.items.add(new SongNode(String.valueOf(i),node));
            }
        }

    }

    public static SongNode formSongNode(Context context, FolderNode root, String song) {
        Uri songUri = Uri.fromFile(new File(song));
        Bitmap thumbnail = parseSongThumbnail(context, songUri);
        int duration = parseSongDuration(context,songUri);
        SongNode node = new SongNode(song,new File(song).getName(), root,thumbnail,duration);
        return node;
    }

    private static List<FolderNode> preOrderFolders(FolderNode node, ArrayList<FolderNode> resultList){
        boolean flag = false;
        for (Node item:node.items
             ) {

        }
        return resultList;
    }

    public static List<FolderNode> getAllFolders(FolderNode root,ArrayList<String> descriptors){
        ArrayList<FolderNode> list = new ArrayList<FolderNode>();
        Queue<FolderNode> queue = new LinkedList<>();
        queue.add(root);
        String curName="";
        while (!queue.isEmpty()){
            FolderNode rt = queue.poll();
            curName += rt.name + "/";
            for (Node node:rt.items
                 ) {
                if (node instanceof FolderNode) {
                    queue.add((FolderNode) node);
                    list.add((FolderNode) node);
                    descriptors.add(curName+node.name+"/");
                }
            }
        }
        return list;

    }
    public static Set<String> getAllSongs(FolderNode root){
        HashSet<String> set = new HashSet<>();
        Queue<FolderNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()){
            FolderNode rt = queue.poll();
            for (Node node:rt.items ) {
                if (node instanceof SongNode){
                    set.add(((SongNode) node).getSrc());
                }
                if (node instanceof FolderNode) {
                    queue.add((FolderNode) node);
                }
            }


    }
        return set;
    }
    public static List<FolderNode> getFolderStructure(FolderNode rootNode){
        ArrayList<FolderNode> list = new ArrayList<>();
        if (rootNode.items!=null){
            for (Node item: rootNode.items
                 ) {
                if (item instanceof FolderNode){
                    list.add((FolderNode) item);
                }
            }
            return list;
        }
        return null;
    }
    @NotNull
    private static int parseSongDuration(Context context, Uri songUri) {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(context.getApplicationContext(),songUri);
        String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        return Integer.parseInt(durationStr);
    }

    private static Bitmap parseSongThumbnail(Context context, Uri songUri) {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        byte[] rawArt;
        Bitmap art;
        BitmapFactory.Options bfo=new BitmapFactory.Options();

        mmr.setDataSource(context.getApplicationContext(), songUri);
        rawArt = mmr.getEmbeddedPicture();
        if (null != rawArt){
            return BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length);
        }
        else return null;
    }

    private static void preOrder(Context context,FolderNode root) {
        ArrayList<FolderNode> list = new ArrayList<FolderNode>();
        Queue<FolderNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            FolderNode rt = queue.poll();
            for (Node node : rt.items
            ) {
                node.setParent(rt);
                if (node instanceof SongNode) {
                    ((SongNode) node).itemBitmap = SongUtils.formBitmapFromByte(context, (SongNode) node);
                }
            }
        }
    }
    public synchronized static void restoreLinks(Context context,FolderNode folderNode) {
        preOrder(context,folderNode);
    }



    // FIXME: 23.05.2021
    public static Object cloneObject(Object obj){
        try{
            Object clone = obj.getClass().newInstance();
            for (Field field : obj.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                field.set(clone, field.get(obj));
            }
            return clone;
        }catch(Exception e){
            return null;
        }
    }


}
