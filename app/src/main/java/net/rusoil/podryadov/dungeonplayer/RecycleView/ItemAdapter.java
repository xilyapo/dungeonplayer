package net.rusoil.podryadov.dungeonplayer.RecycleView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.RippleDrawable;
import android.text.InputType;
import android.transition.Fade;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.dynamicanimation.animation.DynamicAnimation;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;


import net.rusoil.podryadov.dungeonplayer.CustomizeDialogActivity;
import net.rusoil.podryadov.dungeonplayer.ImageAdapter;
import net.rusoil.podryadov.dungeonplayer.MainActivity;
import net.rusoil.podryadov.dungeonplayer.Nodes.NodeChip;
import net.rusoil.podryadov.dungeonplayer.Nodes.FolderNode;
import net.rusoil.podryadov.dungeonplayer.Nodes.Node;
import net.rusoil.podryadov.dungeonplayer.Nodes.PlayListNode;
import net.rusoil.podryadov.dungeonplayer.Nodes.SongNode;
import net.rusoil.podryadov.dungeonplayer.R;
import net.rusoil.podryadov.dungeonplayer.Util.NodeUtils;
import net.rusoil.podryadov.dungeonplayer.Util.SongUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> implements ItemTouchHelperAdapter {
    private final LayoutInflater inflater;
    private List<Node> currentItemList;
    private FolderNode root;
    private FolderNode currentNode;
    int swipedItem = -1;
    MainActivity context;
    boolean isEditMode = true;
    boolean isAllOpen = false;

    public void changeAllOpen() {
        isAllOpen = !isAllOpen;
        notifyDataSetChanged(); }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        Node toNode = currentItemList.get(toPosition);
        Node fromNode = currentItemList.get(fromPosition);
        if (toNode instanceof FolderNode && !(fromNode instanceof FolderNode)) {

        } else {
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(currentItemList, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(currentItemList, i, i - 1);
                }
            }

            notifyItemMoved(fromPosition, toPosition);
        }
    }

    @Override
    public void onItemSwiped(int position, int direction) {
        swipedItem = position;
    }

    public int selectedIndex = -1;

    public ItemAdapter(MainActivity context, FolderNode rootNode) {
        this.inflater = LayoutInflater.from(context);
        this.setCurrentNode(rootNode);
        currentItemList = rootNode.items;
        this.context = context;
        root =rootNode;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_item, parent, false);
        return new ViewHolder(view);
    }

    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Node item = currentItemList.get(position);
        if (item.imageResource != 0)
            holder.icon.setImageResource(item.imageResource);
        else {
            if (item instanceof FolderNode) {
                item.imageResource = R.drawable.ic_folder;
                holder.icon.setImageResource(R.drawable.ic_folder);
            } else if (item instanceof SongNode) {
                holder.icon.setImageResource(R.drawable.ic_song_symbol);
                item.imageResource = R.drawable.ic_song_symbol;
            } else if (item instanceof PlayListNode) {
                item.imageResource = R.drawable.ic_playlist;

                holder.icon.setImageResource(R.drawable.ic_playlist);
            }
        }
        holder.mainText.setText(item.name);
        if (item.backgroundColor != null)
            holder.layout.setBackgroundColor(Color.parseColor(item.backgroundColor));
        if (selectedIndex == position)
            holder.itemView.setBackgroundColor(Color.parseColor("#c7dcff"));
        if (item.nameColor != null)
            holder.mainText.setTextColor(Color.parseColor(item.nameColor));
        if (item instanceof SongNode)
            holder.timeCode.setText(SongUtils.getTimeCode(((SongNode)currentItemList.get(position)).duration));
        holder.timeCode.setVisibility(View.GONE);
        holder.timeCode.setVisibility(View.GONE);
        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAllOpen)
                    holder.showOptions();
                else
                if (holder.isOpen)
                    holder.hideOptions();
                else
                    holder.showOptions();
            }
        });
        holder.icon.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (isEditMode){
                    PopupMenu popup = new PopupMenu(context, holder.timeCode);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.options_menu);
                    //adding click listener

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        int pos = holder.getAdapterPosition();
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            Node currentItem = currentItemList.get(pos);
                            switch (item.getItemId()) {
                                case R.id.change_icon: {
                                    Dialog dialog = new Dialog(context);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setCancelable(false);
                                    dialog.setContentView(R.layout.icon_dialog);
                                    GridView gridView = dialog.findViewById(R.id.icon_grid);
                                    ImageView image = dialog.findViewById(R.id.dialog_currentImage);
                                    image.setImageResource(currentItemList.get(holder.getAdapterPosition()).imageResource);
                                    ImageAdapter adapter = new ImageAdapter(context);
                                    gridView.setAdapter(adapter);
                                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                            adapter.lastSelected = adapter.mThumbIds[position];
                                            image.setImageResource(adapter.lastSelected);

                                        }
                                    });
                                    Button button = dialog.findViewById(R.id.dialog_ok_btn);


                                    button.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            currentItemList.get(pos).imageResource=adapter.lastSelected;
                                            dialog.dismiss();
                                        }
                                    });
                                    dialog.show();
                                    notifyDataSetChanged();
                                }
                                return true;
                                case R.id.change_name: {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setTitle("Введите имя");
                                    final EditText input = new EditText(context);
                                    input.setText(holder.mainText.getText());
                                    input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                                    builder.setView(input);

                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            holder.mainText.setText(input.getText().toString());
                                            currentItemList.get(holder.getAdapterPosition()).name = input.getText().toString();
                                        }
                                    });
                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                                    builder.show();
                                }
                                    return true;
                                case R.id.change_appearance: {
                                    Intent intent = new Intent(context, CustomizeDialogActivity.class);
                                    intent.putExtra("position", holder.getAdapterPosition());
                                    intent.putExtra("text", holder.mainText.getText());
                                    intent.putExtra("textColor", holder.mainText.getCurrentTextColor());
                                    intent.putExtra("backgroundColor", ((ColorDrawable) holder.itemView.getBackground()).getColor());
                                    context.startActivityForResult(intent, MainActivity.REQUEST_CODE_CUSTOM);
                                }
                                    return true;
                                case R.id.remove: {
                                    pos = holder.getAdapterPosition();
                                    currentItem = currentItemList.get(pos);
                                    currentItemList.remove(currentItem);
                                    notifyDataSetChanged();
                                }
                                    return true;
                                case R.id.move_item: {
                                    openMoveDialog(getCurrentNode().items.get(holder.getAdapterPosition()));
                                }
                                    break;
                                case  R.id.make_copy: {
                                    pos = holder.getAdapterPosition();
                                    currentItem = currentItemList.get(pos);
                                    Node newNode;
                                    if (currentItem instanceof FolderNode) {
                                        newNode = ((FolderNode) currentItem).clone(context);
                                        currentItemList.add(pos, newNode);
                                        notifyDataSetChanged();
                                    }
                                    if (currentItem instanceof SongNode) {
                                        newNode = (SongNode) ((SongNode) currentItem).clone();
                                        currentItemList.add(pos, newNode);
                                        notifyDataSetChanged();
                                    }
                                }
                                    break;
                                case R.id.change_tags:
                                {
                                    Dialog dialog = new Dialog(context);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setCancelable(false);
                                    dialog.setContentView(R.layout.tags_edit);


                                    TextView textView = dialog.findViewById(R.id.dialog_input_tag);

                                    ChipGroup chipGroup = dialog.findViewById(R.id.dialog_chipgroup);
                                    for (int i = 0; i<holder.chipGroup.getChildCount();i++){
                                        Chip holderChip = (Chip) holder.chipGroup.getChildAt(i);
                                        Chip chip = new Chip(context);
                                        chip.setText(holderChip.getText());
                                        chip.setTextColor(holderChip.getCurrentTextColor());
                                        chip.setBackground(holderChip.getBackground());
                                        chip.setOnClickListener(new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        chipGroup.removeView(v);
                                                                    }
                                                                });
                                        chipGroup.addView(chip);
                                    }
                                    dialog.setCanceledOnTouchOutside(true);
                                    Button button = dialog.findViewById(R.id.dialog_add_btn);
                                    button.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            com.google.android.material.chip.Chip chip = new com.google.android.material.chip.Chip(context);
                                            chip.setText(textView.getText());
                                            chipGroup.addView(chip);
                                            //holder.chipGroup.addView(chip);
                                        }
                                    });
                                    dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                        @Override
                                        public void onCancel(DialogInterface dialog) {
                                            holder.chipGroup.removeAllViews();
                                            ArrayList<NodeChip> nodeChips = new ArrayList<>();
                                            for (int i = 0; i<chipGroup.getChildCount();i++){
                                                Chip groupChip = (Chip) chipGroup.getChildAt(i);
                                                int textColor = groupChip.getCurrentTextColor();
                                                NodeChip nodeChip = new NodeChip(groupChip.getText().toString(),textColor);
                                                nodeChips.add(nodeChip);
                                            }
                                            currentItemList.get(holder.getAdapterPosition()).nodeChips = nodeChips;
                                            notifyItemChanged(holder.getAdapterPosition());
                                        }
                                    });
                                    dialog.show();
                                }
                                    break;
                                default:
                                    return false;
                            }
                            return true;
                        }
                    });
                    popup.show();
                }
                return false;
            }
        });
        holder.setType(item);
        holder.assignChips(item.nodeChips);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (holder.type) {
                    case FOLDER:
                        setCurrentNode((FolderNode) currentItemList.get(holder.getAdapterPosition()));
                        currentItemList = getCurrentNode().items;
                        context.setCurrentFolderText(getCurrentNode().name);
                        ItemAdapter.super.notifyDataSetChanged();
                        selectedIndex = -1;
                        break;
                    case PLAYLIST:
                        setCurrentNode((FolderNode) currentItemList.get(holder.getAdapterPosition()));
                        currentItemList = ((PlayListNode) getCurrentNode()).items;
                        ItemAdapter.super.notifyDataSetChanged();
                        selectedIndex = -1;
                        break;
                    case SONG:
                        context.playSound(holder.getAdapterPosition(), getCurrentNode().items);
                        selectedIndex = holder.getAdapterPosition();
                        notifyDataSetChanged();
                        //todo: Анимация нажатия


                }
            }
        });
    }

    private void openMoveDialog(Node moveNode) {
        ArrayList<String> descriptors = new ArrayList<>();
        List<FolderNode> list = NodeUtils.getAllFolders(root,descriptors);
        Dialog dialog = new Dialog(context);
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.select_folder_layout);

        TextView text = (TextView) dialog.findViewById(R.id.dialog_select_folder);
        Spinner spinner =  dialog.findViewById(R.id.dialog_spinner);
        String[] stockArr = new String[descriptors.size()];
        stockArr = descriptors.toArray(stockArr);

        ArrayAdapter<String> resultAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1 , descriptors);

        spinner.setAdapter( resultAdapter );
        Button dialogButton = dialog.findViewById(R.id.dialog_select_folder);
        Button cancelButton = dialog.findViewById(R.id.dialog_cancel_select);
        final boolean[] clicked = new boolean[1];
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = spinner.getSelectedItemPosition();
                currentItemList.remove(moveNode);
                FolderNode parent = list.get(pos);
                moveNode.setParent(parent);
                if (moveNode instanceof FolderNode)
                    parent.items.add(0,moveNode);
                else
                    parent.items.add(moveNode);
                notifyDataSetChanged();

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public int getItemCount() {
        return currentItemList.size();
    }

    public boolean back() {
        if (getCurrentNode().getParent() == null)
            return false;
        setCurrentNode(getCurrentNode().getParent());
        currentItemList = getCurrentNode().items;
        context.setCurrentFolderText(getCurrentNode().name);
        notifyDataSetChanged();
        notifyDataSetChanged();
        return true;
    }

    public void changeSelected(int newPos){
        int oldIndex = selectedIndex;
        selectedIndex = newPos;
        notifyItemChanged(oldIndex);
        notifyItemChanged(newPos);
    }

    public void changeCurrentData(int position, String newName, int textColor, int backgroundColor) {
        if (position>-1){
            Node node = currentItemList.get(position);
            node.nameColor = "#"+Integer.toHexString(textColor).substring(2);
            node.backgroundColor = "#"+Integer.toHexString(backgroundColor).substring(2);
            node.name = newName;
            notifyItemChanged(position);
        }
    }

    public FolderNode getCurrentNode() {
        return currentNode;
    }

    public void setCurrentNode(FolderNode currentNode) {
        this.currentNode = currentNode;
        this.currentItemList = currentNode.items;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public String m_Text;
        private ItemTypes type;
        public ImageView icon;
        ImageButton tagsButton;
        ImageButton appPlaylistButton;
        public ChipGroup chipGroup;
        HorizontalScrollView scrollView;
        public TextView mainText;
        TextView timeCode;
        ConstraintLayout layout;
        boolean isOpen = false;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.nav_item_icon);
            tagsButton = itemView.findViewById(R.id.nav_item_tagsSettings);
            appPlaylistButton = itemView.findViewById(R.id.nav_item_addPlaylist);
            chipGroup = itemView.findViewById(R.id.nav_item_itemChipGroup);
            scrollView = itemView.findViewById(R.id.nav_item_chipScrollView);
            mainText = itemView.findViewById(R.id.nav_item_name);
            timeCode = itemView.findViewById(R.id.nav_item_audioInfo);
            layout = itemView.findViewById(R.id.nav_item_layout);
        }

        public void switchState() {
            if (isOpen) {
                isOpen = false;
                hideOptions();
            } else {
                isOpen = true;
                showOptions();
            }
        }

        private void hideOptions() {
            switch (type) {
                case FOLDER:
                    timeCode.setVisibility(View.GONE);
                    //tagsButton.setVisibility(View.GONE);
                    scrollView.setVisibility(View.GONE);
                    //appPlaylistButton.setVisibility(View.GONE);
                    break;
                case PLAYLIST:
                case SONG:
                    timeCode.setVisibility(View.VISIBLE);
                    //tagsButton.setVisibility(View.GONE);
                    scrollView.setVisibility(View.GONE);
                    //appPlaylistButton.setVisibility(View.GONE);
                    break;

                case NONE:
                    break;
            }
            isOpen = false;
        }

        private void showOptions() {
            switch (type) {
                case FOLDER:
                    //tagsButton.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.VISIBLE);
                    break;
                case PLAYLIST:
                case SONG:
                    timeCode.setVisibility(View.VISIBLE);
                    //tagsButton.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.VISIBLE);
                    //appPlaylistButton.setVisibility(View.VISIBLE);
                    break;
            }
            isOpen = true;
        }

        public ItemTypes getType() {
            return type;
        }

        public void setType(Node node) {
            if (node instanceof SongNode) {
                type = ItemTypes.SONG;
            }
            if (node instanceof FolderNode) {
                type = ItemTypes.FOLDER;
            }
            if (node instanceof PlayListNode) {
                type = ItemTypes.PLAYLIST;
            }
            hideOptions();
        }

        public void assignChips(ArrayList<NodeChip> nodeChips) {
            chipGroup.removeAllViews();
            if (nodeChips != null) {
                for (NodeChip item : nodeChips
                ) {
                    com.google.android.material.chip.Chip chip = new com.google.android.material.chip.Chip(this.chipGroup.getContext());
                    chip.setText(item.name);
                    //chip.setBackgroundColor(Color.parseColor(item.backgroundColor));
                    chip.setTextColor(item.textColor);
                    chipGroup.addView(chip);
                }
            }
        }
    }
}
