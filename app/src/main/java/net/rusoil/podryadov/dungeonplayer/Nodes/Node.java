package net.rusoil.podryadov.dungeonplayer.Nodes;


import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;

public class Node implements Serializable {
    @NonNull public static String type;
    protected transient  FolderNode parent;
    public ArrayList<NodeChip> nodeChips;
    public String name;
    public int imageResource;
    public String nameColor;
    public String backgroundColor;
    public FolderNode getParent() {
        return parent;
    }

    public void setParent(FolderNode parent) {
        this.parent = parent;
    }
    public Node() {
        type = "Node";
    }



    public Node(ArrayList<NodeChip> nodeChips, String name, int imageResource, String nameColor, String backgroundColor, FolderNode parent) {
        this.nodeChips = nodeChips;
        this.name = name;
        this.imageResource = imageResource;
        this.nameColor = nameColor;
        this.backgroundColor = backgroundColor;
        this.parent = parent;
        this.type = "Node";

    }

}
