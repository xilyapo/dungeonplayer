package net.rusoil.podryadov.dungeonplayer.RecycleView;

import android.graphics.Color;

public class NavigationItem {
    String name;
    Integer iconResource;
    Integer mainColor;
    Integer textColor;
    Color iconBackgroundColor;


    public ItemTypes getItemType() {
        return itemType;
    }

    protected ItemTypes itemType = ItemTypes.NONE;

    public NavigationItem(String name, int iconResource, ItemTypes itemType) {
        this.name = name;
        this.iconResource = iconResource;
        this.itemType = itemType;
    }

    public NavigationItem(String name, Integer iconResource, Integer mainColor, Integer textColor, Color iconBackgroundColor, boolean isOpen, ItemTypes itemType) {
        this.name = name;
        this.iconResource = iconResource;
        this.mainColor = mainColor;
        this.textColor = textColor;
        this.iconBackgroundColor = iconBackgroundColor;
        this.itemType = itemType;
    }
}
